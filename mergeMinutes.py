import json
from datetime import datetime
from os import listdir

def avg (list):
	list_sum = 0
	for i in list:
		list_sum = list_sum + i
	avg = list_sum / len(list)
	return avg

files = listdir(/home/pi/cyano-automaton/data)
files.remove("right_now.json")

objects=[]

for i in files:
    with open ("/home/pi/cyano-automaton/data/"+files[i], "r") as infile:
        object[i]=json.load(infile)

object[0].temp = [object[0].temp]
object[0].tds = [object[0].temp]
object[0].ph = [object[0].temp]
object[0].ntu = [object[0].temp]

for i in objects:
        object[0].temp = [object[0].temp, object[1].temp]
